### Welcome message ###
#
# Salue les personnes entrant sur les canaux
# Les messages diffèrent selon que ce soit une première arrivée,
# un retour rapide ou long

## ACTIVATION ##
# .chanset #canal +welcome

## CONFIGURATION ##
# 
# Délais
# Il y a deux délais réglables (en minutes).
# Ces délais sont comptés entre la sortie de l'utilisateur et son retour.
#   - time(few) est le délai à partir duquel un utilisateur qui revient
#       sera à nouveau salué
#   - time(long) est le délai à partir duquel on considère qu'il y a
#       longtemps que l'utilisateur est parti.
# Memento: 60min = 1 heure, 1440min = 1 jour, 10080min = 1 semaine
#
# Messages
# Les messages sont de simples textes qui peuvent prendre deux variables:
#   - %1\$s sera remplacé par le pseudo de l'utilisateur
#   - %2\$s sera remplacé par le canal

namespace eval wlc {
	
	variable time
	variable msg
	
	# Temps (en minutes) à partir duquel un retour
	# provoque l'affichage du message "few"
	set time(few) 60
	
	# Temps (en minutes) à partir duquel un retour
	# provoque l'affichage du message "long"
	set time(long) 1440
	
	# Message affiché lorsque l'utilisateur n'a jamais été vu
	# sur le canal
	set msg(first) "Bonjour %1\$s, je suis le robot d'accueil. Les autres utilisateurs ne sont pas loin, tu peux déjà dire bonjour ;)"
	
	# Message affiché lorsque l'utilisateur revient
	# entre "few" minutes et "long" minutes
	set msg(few) "Hello %1\$s, merci de revenir sur %2\$s"
	
	# Message affiché lorsque l'utilisateur revient
	# après plus de "long" minutes
	set msg(long) "Bonjour %1\$s, ça fait plaisir de te revoir ;)"

	# Liste des pseudos à exclure des salutations
	# Peut être sous la forme d'expression régulière
	variable known {_BS*}
	
	## FIN DE CONFIGURATION ##
	
	setudef flag welcome
	
	bind join - * ::wlc::join
	bind part - * ::wlc::part
	bind sign - * ::wlc::part
	
	variable seen
	
	variable author "CrazyCat <http://www.eggdrop.fr>"
	variable version "1.1"
	
	proc join {nick uhost handle chan} {
		if { [string tolower $nick] eq [string tolower $::botnick]} { return }
		if {![channel get [string tolower $chan] welcome] || [channel get [string tolower $chan] welcome] eq ""} { return }
		if {[lsearch -regexp $::wlc::known $nick] != -1} { return } 
		set isback 0
		set uvar "[string tolower $chan],$uhost"
		if {[info exists ::wlc::seen($uvar)]} {
			if {([expr [clock seconds] - $::wlc::seen($uvar)])< ([expr 60 * $::wlc::time(few)])} {
				# Shut up, less than few minutes
				set isback 1
			} elseif {([expr [clock seconds] - $::wlc::seen($uvar)]) < ([expr 60 * $::wlc::time(long)])} {
				# Less than a day, more than few minutes
				putserv "PRIVMSG $chan :[format $::wlc::msg(few) $nick $chan]"
				set isback 1
			} else {
				# More than a day
				putserv "PRIVMSG $chan :[format $::wlc::msg(long) $nick $chan]"
				set isback 0
			}
		} else {
			putserv "PRIVMSG $chan :[format $::wlc::msg(first) $nick $chan]"
		}
		set ::wlc::seen($uvar) [clock seconds]
	}
	
	proc part {nick uhost handle chan {msg ""}} {
		if { [string tolower $nick] eq [string tolower $::botnick]} { return }
		if {![channel get [string tolower $chan] welcome] || [channel get [string tolower $chan] welcome] eq ""} { return }
		if {[lsearch -regexp $::wlc::known $nick] != -1} { return } 
		set uvar "[string tolower $chan],$uhost"
		set ::wlc::seen($uvar) [clock seconds]
	}
	
	putlog "Welcome.tcl v${::wlc::version} by ${::wlc::author} loaded"
}
