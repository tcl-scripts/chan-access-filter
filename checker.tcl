bind ctcr - VERSION version:reply
bind join - "#raspfr *" version:check
bind raw - 378 version:punish

set logfile "databases/idiotbot.log"
set protectedIP "127.0.0.1"

proc log {text} {
	set mylog [open $::logfile a]
	puts $mylog "\[[clock format [clock seconds] -format "%Y-%m-%d %H:%M:%S"]\] $text"
	close $mylog
}

proc version:check {nick uhost handle chan} {
	log "Got $nick and I am $::botnick"
	if {$handle ne "*" || [string tolower $nick] eq [string tolower $::botnick]} { return 0 }
	log "Checking $nick"
	set ::cversion([string tolower $nick]) 1
	putserv "PRIVMSG $nick :\001VERSION\001"
	utimer 15 [list version:none $nick $uhost $chan]
}

proc version:reply {nick uhost handle dest kw arg} {
	if {[string tolower $nick] eq [string tolower $::botnick]} { return 0 }
	if {[info exists ::cversion([string tolower $nick])]} {
		unset ::cversion([string tolower $nick])
		log "Version for $nick is $arg"
	}
}

proc version:none {nick uhost chan} {
	if {[string tolower $nick] eq [string tolower $::botnick]} { return 0 }
	if {[info exists ::cversion([string tolower $nick])] && [onchan $nick $chan]} {
		log "Preparing $nick on $chan"
		putquick "WHOIS $nick"
		unset ::cversion([string tolower $nick])
	}
}

proc version:punish {from kw text} {
	set host [lindex [split $text " "] end]
	if { $host eq $::protectedIP } { return 0 }
	putserv "gzline *@$host +7d :Suspicious bot"
	log "gzline *@$host +7d :Suspicious bot"
}
