################################
#      Chan Access Filter      #
################################

# This script checks ASV (in realname)
# and bans people if they don't match
# the required age or sex

# Settings are done from party-line
# .caf #chan sex <U/F/M/T> to set sex restriction (U => no restriction)
# .caf #chan min [0-99] to set minimal age (0 or empty => no minimal age)
# .caf #chan max [0-99] to set maximal age (0 or empty => no maximal age)
#
# Add the +opwarn flag to the chan to get warning in wallop
#
# you can also use the direct setting (deprecated - no check)
# .chanset #chan caf sex:min:max
# with:
#   sex: U (no restriction), M, F, T
#        T is allowed in M & F chans, but M & F are not allowed in T chans
#   min: integer 0 (no restriction) to 99
#   max: integer 0 (no restriction) to 99
#
## TODOLIST ##
# - if maxage is lower than minage, it's unused
# - add variable ban/kick reason (and translation)
#
## About the author ##
# CrazyCat <crazycat [at] c-p-f.org>
# irc://irc.zeolia.net/eggdrop
# https://www.zeolia.net/canaux/eggdrop.html

namespace eval caf {
	
	# Type of ban
	variable bantype 3

	# Delay before check is done (in seconds)
    variable warndelay 10
	# Warning message
	variable warnmsg "Votre ASV n'est pas indiqué. Tapez \002/setname age/sexe/ville\002 pour le renseigner (sexe: M, F ou T)"
	# Delay for recheck (and kick/ban - in seconds)
	variable kickdelay 50
	# Warning age: 
	variable agewarn 18
	# Wallop ASV ?
	variable wopasv 1

	# Activate the debug log
	variable debug 1
	
	######################################################################
	# DO NOT EDIT
	######################################################################
	
	setudef str caf
	setudef flag opwarn
	
	variable umem
	variable uwarn
	variable udatas
	variable author "CrazyCat"
	variable version "2.1"
	
	bind raw - 352 ::caf::checkuser
	bind dcc - caf ::caf::setter
	
	# Run a WHO command on join
    # Used only if extended-join is not available
	proc whoisuser {nick uhost handle chan} {
		if { [isbotnick $nick] } { return 0 }
		if { [matchattr $handle l] || [matchattr $handle h $chan] || [matchattr $handle f]} { return 0 }
		if { [isop $nick $chan] || [ishalfop $nick $chan] || [isvoice $nick $chan]} { return 0 }
		if { [::caf::chanisrestricted $chan] == 1 || [channel get $chan opwarn]} {
			if {([lsearch -nocase [array names ::caf::umem] [string tolower $nick]]==-1) || ([llength $::caf::umem([string tolower $nick])]==0)} {
				# new user, or user already checked in all previous channels
				set ::caf::umem([string tolower $nick]) $chan
				putlog "Ready to scan $nick"
				after [expr $::caf::warndelay * 1000] {set whois ok}
				vwait whois
				putlog "Scanning $nick"
				putserv "WHO $nick"
			} else {
				if {[lsearch $::caf::umem([string tolower $nick]) $chan]==-1} {
					# known user joins a new channel
					lappend ::caf::umem([string tolower $nick]) $chan
				}
			}
		}
		return 0
	}
	
    # Use extended-join to perform the first test
    proc cafjoin {from kw text flag} {
        regexp -- {(.+)!(.+@.+)} $from - nick uhost
        regexp -- {([^\s]+)\s([^\s]+)\s:(.+)} $text - chan account realname
        set handle [nick2hand $nick $chan]
        set host [join [lindex [split $from {!}] 1]]
        set target [::caf::parsename $host $realname]
        if { [isbotnick $nick] } { return 0 }
		if { [matchattr $handle l] || [matchattr $handle h $chan] || [matchattr $handle f]} { return 0 }
		if { [isop $nick $chan] || [ishalfop $nick $chan] || [isvoice $nick $chan]} { return 0 }
        if { [::caf::chanisrestricted $chan] == 1 || [channel get $chan opwarn]} {
            if {[info exists ::caf::wopasv] && $::caf::wopasv==1} {
				putquick "NOTICE %$chan :Extend join : $nick asv $realname"
			}
			if {[channel get $chan opwarn] && [info exists ::caf::agewarn]} {
				set min [expr $::caf::agewarn * 1]
				if {[lindex $target 0]<$min} {
					putlog "send warn"
					putquick "NOTICE %$chan :\00304Danger\003: $nick is only \00302[lindex $target 0] old\003"
				} else {
					putlog "[lindex $target 0] >= $min"
				}
			}
			if {[::caf::chanisrestricted $chan] == 0} { return }
            if {[lindex $target 3] eq "N"} {
                # Invalid ASV -> run /WHO
                putquick "NOTICE $nick :$::caf::warnmsg"
				lappend ::caf::uwarn([string tolower $nick]) $chan
				set ::caf::umem([string tolower $nick]) [lreplace $::caf::umem([string tolower $nick]) [lsearch $::caf::umem([string tolower $nick]) $chan] [lsearch $::caf::umem([string tolower $nick]) $chan]]
				after [expr $::caf::kickdelay * 1000] {set whois ok}
				vwait whois
				putlog "Re-Scanning $nick"
				putserv "WHO $nick"
                return
            }
			if {[lindex $target 3] eq "Y" && [::caf::uservsrestrict $target [channel get $chan caf]] == 0} { 
                return
            } else {
                dlog "newchanban $chan $nick [::caf::mask $host $nick]"
				newchanban $chan [::caf::mask $host $nick] $::botnick "Chan access filter - Denied access" 60
				putkick $chan $nick "Chan access filter - Denied access"
            }
        }
    }
    
	# parse the WHO response and treat it
	proc checkuser {from keyword text} {
		set realname [join [lrange [set uinfos [split $text]] 8 end]]
		set nick [lindex $uinfos 5]
		if { [isbotnick $nick] } { return 0 }
		if { [lsearch -nocase [array names ::caf::umem] [string tolower $nick]] == -1 && [lsearch -nocase [array names ::caf::uwarn] [string tolower $nick]] == -1 } { return 0 }
		set handle [nick2hand $nick]
		if { [matchattr $handle l] || [matchattr $handle f]} { return 0 }
		set host "[lindex $uinfos 2]@[lindex $uinfos 3]"
		set target [::caf::parsename $host $realname]
		foreach chan [channels] {
			if {![onchan $nick $chan]} { continue }
			if {[info exists ::caf::wopasv] && $::caf::wopasv==1} {
				putquick "NOTICE %$chan :$nick asv $realname"
			}
			if {[channel get $chan opwarn] && [info exists ::caf::agewarn]} {
				set min [expr $::caf::agewarn * 1]
				if {[lindex $target 0]<$min} {
					putlog "send warn"
					putquick "NOTICE %$chan :\00304Danger\003: $nick is only \00302[lindex $target 0] old\003"
				} else {
					putlog "[lindex $target 0] >= $min"
				}
			}
			if {[::caf::chanisrestricted $chan] == 0} { continue }
			if {[::caf::uservsrestrict $target [channel get $chan caf]] == 0} { continue }
			if {([lsearch [array names ::caf::uwarn] [string tolower $nick]] != -1) && ([lsearch $::caf::uwarn([string tolower $nick]) $chan]!=-1)} {
				dlog "newchanban $chan $nick [::caf::mask $host $nick]"
				newchanban $chan [::caf::mask $host $nick] $::botnick "Chan access filter - Denied access" 60
				putkick $chan $nick "Chan access filter - Denied access"
				set ::caf::uwarn([string tolower $nick]) [lreplace $::caf::uwarn([string tolower $nick]) [lsearch $::caf::uwarn([string tolower $nick]) $chan] [lsearch $::caf::uwarn([string tolower $nick]) $chan]]
			} else {
				putquick "NOTICE $nick :$::caf::warnmsg"
				# uwarn must be for each channel
				lappend ::caf::uwarn([string tolower $nick]) $chan
				set ::caf::umem([string tolower $nick]) [lreplace $::caf::umem([string tolower $nick]) [lsearch $::caf::umem([string tolower $nick]) $chan] [lsearch $::caf::umem([string tolower $nick]) $chan]]
				after [expr $::caf::kickdelay * 1000] {set whois ok}
				vwait whois
				putlog "Re-Scanning $nick"
				putserv "WHO $nick"
			}
		}
		
		return 0
	}
	
	proc parsename {host realname} {
		putlog "Checking $realname"
		if { ![regexp {\[?(\d{1,2}) ?/? ?(\w) ?/? ?([^\]]*)\]?} $realname uall uage usex uloc] } {
			set ::caf::udatas($host) [list 0 U "" N]
		} else {
			set ::caf::udatas($host) [list $uage $usex $uloc Y]
		}
		return $::caf::udatas($host)
	}
	
	# Compares user AS and chan restrictions
	proc uservsrestrict {target restrict} {
		regexp {(\w)?:(\d{1,2})?:(\d{1,2})?} $restrict call csex cmin cmax
		if { $cmin ne "" || [expr $cmin * 1] > 0 } { 
			if {![string is integer -strict [lindex $target 0]] || [lindex $target 0]<$cmin} {
				dlog "Too young for $target"
				return 1
			}
		}
		if { $cmax ne "" && [expr $cmax * 1] > 0 } { 
			if {![string is integer -strict [lindex $target 0]] || [lindex $target 0]>$cmax} {
				dlog "Too old for $target"
				return 1
			}
		}
		if {$csex ne "U" && [string toupper [lindex $target 1]] ne $csex} {
			if {[string toupper [lindex $target 1]] eq "T" && ($csex eq "M" || $csex eq "F")} {
				return 0
			}
			dlog "bad gender for $target"
			return 1
		}
		return 0
	}
	
	# Returns 1 if chan has restriction
	proc chanisrestricted { chan } {
		set restrict [channel get $chan caf]
		if {$restrict eq "" || $restrict eq "U:0:0"} { return 0; }
		regexp {(\w)?:(\d{1,2})?:(\d{1,2})?} $restrict all sex min max
		set check 0
		if { $sex eq "F" || $sex eq "M" || $sex eq "T"} { set check 1 }
		if { [string is integer -strict $min] && [expr $min * 1] > 0 } { set check 1 }
		if { [string is integer -strict $max] && [expr $max * 1] > 0 } { set check 1 }
		return $check
	}
	
	# Global setter: checks syntax and formats datas
	proc setter {handle idx text} {
		set args [split $text]
		if {[llength $args]<2} {
			putlog "Syntax: .caf #chan sex|min|max \[value\]"
			return 0
		}
		set chan [string tolower [lindex $args 0]]
		if {[lsearch [string tolower [channels]] $chan]==-1} {
			putlog "Sorry, I don't know the channel $chan"
			return 0
		}
		if {[llength $args]==3} {
			set nval [lindex $args 2]
		} else {
			set nval ""
		}
		switch [string tolower [lindex $args 1]] {
			sex -
			min -
			max {
				::caf::pset [string tolower [lindex $args 1]] $chan $nval
				return 0
			}
			default {
				putlog "Syntax: .caf #chan sex|min|max \[value\]"
				return 0
			}
		}
	}
	
	# Sets parameters (recreate the setting)
	proc pset {type chan {arg 0}} {
		set restrict [channel get $chan caf]
		if { $restrict eq "" } { set restrict "U:0:0" }
		regexp {(\w)?:(\d{1,2})?:(\d{1,2})?} $restrict all sex min max
		if { $min eq "" } { set min 0 } else { set min [expr $min * 1] }
		if { $max eq "" } { set max 0 } else { set max [expr $max * 1] }
		if {$type eq "sex"} {
			switch [string toupper $arg] {
				"F" -
				"M" -
				"T" {
					set sex [string toupper $arg]
				}
				default {
					set sex "U"
				}
			}
		}
		if {$type eq "min" || $type eq "max"} {
			if { $arg eq "" } { set age 0 }  else { set age [expr $arg * 1] }
			if {$age<1 || $age>99} {
				set age 0
			}
			if {$type eq "min"} {
				set min $age
			} else {
				set max $age
			}
		}
		channel set $chan "caf" "$sex:$min:$max"
	}
	
	# shortcut to log
	proc dlog {msg} {
		if {$::caf::debug == 1} {
			putlog "Debug : $msg"
		}
	}
	
	# Generates the banmask
	proc mask {uhost nick} {
        switch -- $::caf::bantype {
            1 { set mask "*!*@[lindex [split $uhost @] 1]" }
            2 { set mask "*!*@[lindex [split [maskhost $uhost] "@"] 1]" }
            3 { set mask "*!*$uhost" }
            4 { set mask "*!*[lindex [split [maskhost $uhost] "!"] 1]" }
            5 { set mask "*!*[lindex [split $uhost "@"] 0]*@[lindex [split $uhost "@"] 1]" }
            6 { set mask "*$nick*!*@[lindex [split [maskhost $uhost] "@"] 1]" }
            7 { set mask "*$nick*!*@[lindex [split $uhost "@"] 1]" }
            8 { set mask "$nick![lindex [split $uhost "@"] 0]@[lindex [split $uhost @] 1]" }
            9 { set mask "$nick![lindex [split $uhost "@"] 0]@[lindex [split [maskhost $uhost] "@"] 1]" }
            default { set mask "*!*@[lindex [split $uhost @] 1]" }
        }
        return $mask
    }
	
	proc init {} {
        if {[string match -nocase *extended-join* [cap ls]]} {
            if {![string match *extended-join* [cap enabled]]} {
                cap req extended-join
            }
            bind RAWT - JOIN ::caf::cafjoin
        } else {
            bind join - * ::caf::whoisuser
        }
		foreach chan [channels] {
			if { [channel get $chan caf] eq "" } { channel set $chan caf "U:0:0" }
		}
		putlog "Channel Access Filter V$::caf::version by $::caf::author loaded"
	}
}

::caf::init
